[![pipeline status](https://gitlab.com/QX4MX/vidsync-legacy/badges/master/pipeline.svg)](https://gitlab.com/QX4MX/vidsync-legacy/pipelines)

 [New Vidsync Repository](https://gitlab.com/QX4MX/vidsync)
## local with node

### install dependencies 
* make sure [node.js](https://nodejs.org/en/download/) is installed and added to path
* `npm install`

### launch on localhost
* `npm run setup` to build js, css and add resources to dist
* `npm run launch` to launch on localhost:80
app will be on localhost:80 (make sure js and css are already build)

### build project files
* `npm run build-js-watch` for continuous js building
* `npm run build-css-watch` for continuous css building

* `npm run setup` (for html, img and lib changes)

You can also use the specific commands which can be found in [package.json ](https://gitlab.com/QX4MX/vidsync-legacy/blob/master/package.json) -> scripts

## local with docker
* make sure docker is installed and added to path 
* `docker-compose up --build` to launch on localhost:80


## kubernetes with helm
* make sure kubernetes is setup, helm installed
* `helm install vidchart`


