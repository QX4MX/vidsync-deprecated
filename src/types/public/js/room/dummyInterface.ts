interface Document {

    mozFullScreenElement: any;
    mozCancelFullScreen: any;
    mozRequestFullScreen: any;
    webkitRequestFullscreen: any;
    webkitFullscreenElement: any;
    webkitExitFullscreen: any;
    webkitCancelFullScreen: any;
}