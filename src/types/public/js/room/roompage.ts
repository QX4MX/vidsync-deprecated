let roomId = "";
let isPerm = false;
let isPublic = false;
let isEditable = false;
let wantSync = false;

//create player
let ytplayer: any;
let player: Player = new Player(ytplayer);

let tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
let firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onYouTubeIframeAPIReady() {
    ytplayer = new YT.Player('player', {
        width: 1280,
        height: 720,
        videoId: '',
        playerVars: { 'autoplay': 1, 'controls': 0 },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

function onPlayerReady(event: any) {
    player = new Player(ytplayer);
    player.setReady(true);
    player.setVolume(10);
}

function onPlayerStateChange(event: any) {
    if(event.data != this.lastState){
        if (event.data == YT.PlayerState.PLAYING) {
            player.setIsPaused(false);
            $('#play-video').hide();
            $('#pause-video').show();
            $('#playTime').attr("max", player.getDuration());
            timeToHtml(player.getDuration(), '#durationT');
        }
        if (event.data == YT.PlayerState.PAUSED) {
            player.setIsPaused(true);
            $('#play-video').show();
            $('#pause-video').hide();
        }
        if(event.data == YT.PlayerState.UNSTARTED){
            player.setIsPaused(true);
            $('#play-video').show();
            $('#pause-video').hide();
        }
        if (event.data == YT.PlayerState.ENDED) {
            player.setIsPaused(true);
            if (player.getCurrentTime() == player.getDuration()) {
                socketClass.emit(SocketEvent.FINISH);
            }
        }
    }
    this.lastState = event.data;
}


$(document).mousemove(function () {
    vidctrlShow();
});

let joinInterval = setInterval(function () {
    if (socketClass.getIsReady() && player.getIsReady()) {
        roomId = getParamFromUrl(window.location.href, "id");
        if (getParamFromUrl(window.location.href, "pub") == 'true') {
            isPublic = true;
        }
        if (getParamFromUrl(window.location.href, "perm") == 'true') {
            isPerm = true;
        }
        if (getParamFromUrl(window.location.href, "e") == 'true') {
            isEditable = true;
        }
        socketClass.join(roomId, isPerm, isPublic, isEditable)
        socket = socketClass.getSocket();
        clearInterval(joinInterval);
    }
}, 500)

setInterval(function () {
    if (player.getIsReady()) {
        $('#playTime').val(player.getCurrentTime());
        timeToHtml(player.getCurrentTime(), '#currentT');
    }
    $('#messages').height($('#playerdiv').height() - $('#msgForm').height() - $('#chattop').height());
}, 100);

$('#resultwrapper').hide();

$('#play-video').click(function () {
    socketClass.emit(SocketEvent.PLAY);
});

$('#stop-video').click(function () {
    socketClass.emit(SocketEvent.STOP);
});

$('#pause-video').click(function () {
    socketClass.emit(SocketEvent.PAUSE);
});

$('#next-video').click(function () {
    socket.emit(SocketEvent.NEXT);
});

$("#volume").on("input change", function () {
    let value = <number>$('#volume').val();
    player.setVolume(value);
})

$('#mute-video').click(function () {
    player.mute();
});

// leave change so it doesnt send event more than once
$('#playTime').change(function () {
    let value = <number>$('#playTime').val();
    socketClass.emitNumber(SocketEvent.SYNCTIME, value);
});

$('#addToQ').click(function () {
    let urlString = <string>$('#addQueId').val();
    let videoId = player.getVideoId(urlString);
    socket.emit(SocketEvent.ADD_TO_QUEUE, videoId);
    $('#addQueId').val('');
});

$('#searchYoutubebtn').click(function () {
    let ytsearchterm = <string>$('#ytsearchterm').val();
    socket.emit(SocketEvent.searchYT, ytsearchterm);
    $('#addQueId').val('');
});

$('#searchPlaylistVideos').click(function () {
    let playlistid = getParamFromUrl(<string>$('#playlistId').val(),'list');
    if(playlistid != null){
        socket.emit(SocketEvent.playlistVideos, playlistid);
        $('#playlistId').val('');
    }
    else{
        alert("Please Enter valid url")
    }
});

$('#newUserName').click(function () {
    let newUserName = $('#newUserNameVal').val();
    socket.emit(SocketEvent.SET_USERNAME, newUserName);
});

$('#sync-me').click(function () {
    socket.emit(SocketEvent.SYNC_THIS);
    wantSync = true;
});

$('#sendMsg').click(function () {
    socket.emit(SocketEvent.MSG, $('#m').val());
    $('#m').val('');
});

$('#link').click(function () {
    copyToClipboard(window.location.origin + '/room?id=' + roomId);
});

$('#vidUrl').click(function () {
    copyToClipboard(player.getVideoUrl());
});


$(document).on("click", "button.playBtn", function () {
    let listElemID = $(this).parent().attr('id');
    let listElemNum = parseInt(listElemID);
    console.log("play qelem " + listElemNum);
    socket.emit(SocketEvent.PLAY_QUEUE_ELEM, listElemNum);
});
$(document).on("click", "button.removeBtn", function () {
    let listElemID = $(this).parent().attr('id');
    let listElemNum = parseInt(listElemID);
    console.log("remove qelem " + listElemNum);
    socket.emit(SocketEvent.RM_QUEUE_ELEM, listElemNum);
});

$(document).on("click", "button.addBtn", function () {
    let videoid = $(this).parent().attr('id');
    console.log(videoid);
    socket.emit(SocketEvent.ADD_TO_QUEUE, videoid);
});

$(document).on("click", "button.dismissBtn", function () {
    $(this).parent().remove();
});

$(document).on("click", "button.closeResults", function () {
    $('#resultwrapper').hide();
});

$(document).on("click", "button.addAllResults", function () {
    var listItems = $("#playlistVids li");
    for (let li of listItems) {
        socket.emit(SocketEvent.ADD_TO_QUEUE, li.id);
    }
});

$(document).on("click", "button.showRelated", function () {
    socket.emit(SocketEvent.LOAD_RELATED);
});

$('#fullscreen').on("click", function () {
    player.goFullScreen()
});

// Keyboard commands
$('html').keydown(function (e) {  //keypress did not work with ESC;
    if(!$("input").is(":focus")){
        e.preventDefault();
        // toggle play/pause (space)
        if(e.keyCode == 32 ){
            if(player.getIsPaused()){
                socketClass.emit(SocketEvent.PLAY);
            }
            else{
                socketClass.emit(SocketEvent.PAUSE);
            }
        }
        // skip forward (arrow key right)
        else if(e.keyCode == 39 ){
            let value = <number>$('#playTime').val();
            console.log(value);
            // + acts like string ?
            value -= -5;
            console.log(value);
            socketClass.emitNumber(SocketEvent.SYNCTIME, value); 
        }
        // skip back (arrow key left)
        else if(e.keyCode == 37 ){
            let value = <number>$('#playTime').val() ;
            console.log(value);
            value -= 5;
            console.log(value);
            socketClass.emitNumber(SocketEvent.SYNCTIME, value); 
        }
        // volume up (arrowkey up)
        else if(e.keyCode == 38 ){
            player.setVolume(player.getVolume()+5);
        }
        // volume down (arrowkey down)
        else if(e.keyCode == 40 ){
            player.setVolume(player.getVolume()-5);
        }
        // mute (m)
        else if(e.keyCode == 77 ){
            player.mute();
        }
        
        // fullscreen (f)
        else if (e.keyCode == 70) {
            player.goFullScreen();
        }
    }
});

$('form').submit(function (e) {
    e.preventDefault(); // prevents page reloading
    return false;
});


socket.on(SocketEvent.UPDATE, function (count: number, currentroom: string) {
    $('#onlineCount').html("Users : " + count);
    $('#currentRoom').html("Room : " + currentroom);
    $('#roomVal').val(currentroom);
});

socket.on(SocketEvent.LOAD_VID, function (id: string) {
    player.setVideo(id);
    $('#lastPlayerEvent').html("Set New Video");
});

socket.on(SocketEvent.PLAY, function () {
    player.play();
    $('#lastPlayerEvent').html("Playing");
});

socket.on(SocketEvent.STOP, function () {
    player.stop();
    $('#lastPlayerEvent').html("Stop");
});

socket.on(SocketEvent.PAUSE, function () {
    player.pause();
    $('#lastPlayerEvent').html("Paused");
});

socket.on(SocketEvent.SYNCTIME, function (time: number) {
    player.skipTo(time);
    socketClass.wantSync = false;
    $('#lastPlayerEvent').html("Skip to " + time);
});

socket.on(SocketEvent.LOAD_QUEUE, function (queue: string[], length: number) {
    $('#queue').empty();
    for (let i = 0; i < length; i++) {
        let vidUrlString = 'https://www.youtube.com/watch?v=' + queue[i];
        $('#queue').append("<li id=" + i + "><button class='removeBtn bg-darker c-primary border-primary hover-bg-primary'>X</button><button class='playBtn bg-darker c-primary border-primary hover-bg-primary'>Play</button><img id='video-list-" + i + "'src='https://img.youtube.com/vi/" + queue[i] + "/hqdefault.jpg'></li>");
    }
    $('#upnext').html("<iframe class='embed-responsive-item' src='https://www.youtube.com/embed/" + queue[0] + "'></iframe>");

    /* <div class='d-flex'>\
    <button type='button' class='playBtn' id='"+ i + "'>Play Video</button>\
    <button type='button' class='removeBtn' id='"+ i + "'>X</button>\
    </div> */
});

socket.on(SocketEvent.searchYT, function (success: boolean, result: Array<Array<string>>) {
    $('#resultwrapper').show();
    $('#searchResults').empty();
    if (success) {
        for (let item of result) {
            $('#searchResults').append("<li id=" + item[0].toString() + "><span class='resultTitle'>" + item[1].toString() + "</span><button class='dismissBtn bg-darker c-primary border-primary hover-bg-primary'>X</button><button class='addBtn bg-darker c-primary border-primary hover-bg-primary'>Add</button><img src='https://img.youtube.com/vi/" + item[0].toString() + "/hqdefault.jpg'></li>")
        }
    }
    else {
        console.log("Unable to get your Searchrequest");
    }
});

socket.on(SocketEvent.playlistVideos, function (result: Array<Array<string>>) {
    $('#playlistVids').empty();
    if (result) {
        for (let item of result) {
            $('#playlistVids').append("<li id=" + item[0].toString() + "><span class='resultTitle'>" + item[1].toString() + "</span><button class='dismissBtn bg-darker c-primary border-primary hover-bg-primary'>X</button><button class='addBtn bg-darker c-primary border-primary hover-bg-primary'>Add</button><img src='https://img.youtube.com/vi/" + item[0].toString() + "/hqdefault.jpg'></li>")
        }
    }
});

socket.on(SocketEvent.VIDSTATS, function (result: Array<string>) {
    $('#vidTitle').html(result[0]);
    $('#vidChannel').html('<i class="fas fa-user-circle"></i> '+result[1]);
    $('#vidViews').html('<i class="fas fa-eye"></i> '+result[2]);
    $('#vidLikes').html('<i class="fas fa-thumbs-up"></i> '+result[3]);
    $('#vidDislikes').html('<i class="fas fa-thumbs-down"></i> '+result[4]);
    $('#vidPostedTime').html('<i class="fas fa-clock"></i> '+result[5]);
});

socket.on(SocketEvent.RELATEDVIDS, function (result: Array<Array<string>>) {
    $('#related').empty();
    if (result) {
        for (let item of result) {
            $('#related').append("<li id=" + item[0].toString() + "><span class='resultTitle'>" + item[1].toString() + "</span><button class='dismissBtn bg-darker c-primary border-primary hover-bg-primary'>X</button><button class='addBtn bg-darker c-primary border-primary hover-bg-primary'>Add</button><img src='https://img.youtube.com/vi/" + item[0].toString() + "/hqdefault.jpg'></li>")
        }
    }
});

socket.on(SocketEvent.MSG, function (msg: string, author: string) {
    let currentdate = new Date();
    let textColor = "text-white";
    let bgColor = "bg-darker";
    if (author == "SYSTEM") {
        textColor = "c-primary";
    }
    else if (author == "ROOM") {
        textColor = "c-secondary";
    }
    else if (author == "INFO") {
        textColor = "c-info"
    }
    if ($('#messages li').length % 2 == 0) {
        bgColor = "bg-dark";
    }
    $('#messages').append("<li class='" + bgColor + "'><span class='author " + textColor + "'>" + author + "</span><span class='msg'>" + msg + "</span><span class='msgtime text-right'>" + currentdate.getHours() + ':' + currentdate.getMinutes() + ':' + currentdate.getSeconds() + "</span></li>");
    // scroll down
    let element = document.getElementById("messages");
    element.scrollTop = element.scrollHeight;
});

socket.on(SocketEvent.SYNC_OTHER, function () {
    if (!socketClass.wantSync) {
        socket.emit(SocketEvent.SYNC_OTHER, player.getCurrentTime());
    }
});

socket.on(SocketEvent.BACKTOINDEX,function(){
    confirm("This RoomName is not allowed, please recreate a valid Room");
    window.location.href = '/'; 
});

