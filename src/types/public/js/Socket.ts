class Socket {
    private socket: SocketIOClient.Socket;
    public isReady: boolean = false;
    public wantSync: boolean = false;

    constructor(id: string, username: string) {
        this.socket = io.connect(window.location.origin, {
            query: {
                userId: id,
                username: username
            }
        });
    }

    getIsReady() {
        return this.socket.connected;
    }

    getSocket() {
        return this.socket;
    }

    join(roomId: string, isPerm: boolean, isPublic: boolean, isEditable: boolean) {
        this.socket.emit(SocketEvent.JOIN, roomId, isPerm, isPublic, isEditable);
    }

    emit(str: string) {
        this.socket.emit(str);
    }

    emitNumber(str: string, value: number) {
        this.socket.emit(str, value);
    }

}

let cookie = new Cookie();
let username = cookie.getUserName();
let id = cookie.getId();

let socketClass = new Socket(id, username);
let socket = socketClass.getSocket();;


socket.on(SocketEvent.ALERT, function (msg: string) {
    alert(msg);
});

socket.on(SocketEvent.SET_ID, function (id: string) {
    cookie.setCookie("id", id, 365);
});

socket.on(SocketEvent.SET_USERNAME, function (success: boolean, newUN: string) {
    if (success) {
        cookie.setCookie("username", newUN, 365);
        $('#currentUserNameHtml').html("UserName: " + newUN);
    }
});
