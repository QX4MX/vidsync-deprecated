import { SystemUserName, SocketEvent, Errors, ChatCommand } from "./Enums";
import { User } from "./User";
import { Room } from "./Room";

export class ChatEvents {
    private io: SocketIO.Server;
    constructor(io: SocketIO.Server) {
        this.io = io;
    }

    systemMsg(msg: string, from: string): void {
        if (from == null) {
            from = SystemUserName.SYSTEM;
        }
        this.io.emit(SocketEvent.MSG, msg, from);
    }

    roomMsg(room: string, msg: string, from: string): void {
        if (from == null) {
            from = SystemUserName.ROOM;
        }
        this.io.to(room).emit(SocketEvent.MSG, msg, from);
    }

    socketMsg(socket: SocketIO.Socket, msg: string, from: string) {
        if (from == null) {
            from = "";
        }
        socket.emit(SocketEvent.MSG, msg, from);
    }

    OnUserMessage(socket: SocketIO.Socket, msg: string, user: User, room: Room) {
        if (room) {
            let roomEdit = room.getIsEditable();
            if (!msg || msg.length === 0 || msg.charAt(0) === "" || !(/\S/.test(msg))) {
                socket.emit(SocketEvent.MSG, Errors.EMPTYMSG, SystemUserName.SYSTEM);
            } else {
                this.roomMsg(user.getCurrentRoom(), msg, user.getUserName());
            }
        }
    }

    //TODO chat commands
}
