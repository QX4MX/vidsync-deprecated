import { createServer, Server } from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';
import * as path from "path";

import { UserEvents } from './UserEvents';
import { Routing } from './Routing';
import { SocketEvent, SystemUserName, Errors } from "./Enums";
import { PlayerEvents } from './PlayerEvents';
import { RoomData } from './RoomData';
import { UserData } from './UserData';
import { ChatEvents } from './ChatEvents';
import { youtubeapi } from './youtubeapi';

export class AppServer {
    public static readonly PORT: number = 80;
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;
    private router: any = express.Router();

    private userevent: UserEvents;
    private playerevent: PlayerEvents;
    private routing: Routing;

    private chat: ChatEvents;
    private rooms: RoomData = new RoomData();
    private users: UserData = new UserData();
    private ytApi: youtubeapi = new youtubeapi();

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.createDataHandling();
        this.createRouting();
        this.listen();
    }

    private createApp(): void {
        this.app = express();
        this.app.use(express.static(path.resolve('./dist/public/')));
        //add the router
        this.app.use('/', this.router);
    }

    private createServer(): void {
        this.server = createServer(this.app);
    }
    private createDataHandling() {
        this.chat = new ChatEvents(this.io);
        this.userevent = new UserEvents(this.io, this.chat, this.rooms, this.users, this.ytApi);
        this.playerevent = new PlayerEvents(this.io, this.chat, this.rooms, this.users, this.ytApi);
    }
    private createRouting() {
        this.routing = new Routing(this.userevent.getAdmin(), this.ytApi);
    }

    private config(): void {
        this.port = process.env.PORT || AppServer.PORT;
    }

    private sockets(): void {
        this.io = socketIo(this.server);
    }

    private listen(): void {
        this.server.listen(this.port, () => {
            console.log('Running server on port %s', this.port);
        });

        this.app.get('*', (req, res) => { this.routing.get(req, res) });
        this.app.post('*', (req, res) => { this.routing.post(req, res) });

        this.io.on(SocketEvent.CONNECT, (socket: SocketIO.Socket) => {
            this.userevent.userConnect(socket);
            // User Events
            socket.on(SocketEvent.DISCONNECT, () => { this.userevent.userDisconnect(socket); });
            socket.on(SocketEvent.SET_USERNAME, (newUN: string) => { this.userevent.userSetUserName(socket, newUN); });
            socket.on(SocketEvent.JOIN, (roomID: string, isPerm: boolean, isPublic: boolean, isEditable: boolean) => { this.userevent.userJoinRoom(socket, roomID, isPerm, isPublic, isEditable) });
            socket.on(SocketEvent.PUBLIC_ROOMS, () => { this.userevent.getPublicRooms(socket); });
            socket.on(SocketEvent.GETUSERS, (pw: string) => { this.userevent.adminGetUsers(socket, pw) });
            socket.on(SocketEvent.GETROOMS, (pw: string) => { this.userevent.adminGetRooms(socket, pw) });
            socket.on(SocketEvent.DELROOM, (pw: string, roomID: string) => { this.userevent.adminDeleteRoom(socket, pw, roomID) });
            socket.on(SocketEvent.DELUSER, (pw: string, username: string) => { this.userevent.adminDeleteUser(socket, pw, username) });

            socket.on(SocketEvent.SYNCTIME, (time: string) => { this.userevent.syncTime(socket, time); });
            socket.on(SocketEvent.SYNC_THIS, () => { this.userevent.userSync(socket); });
            socket.on(SocketEvent.SYNC_OTHER, (time: number) => { this.userevent.syncUser(socket, time); });
            // Player Events          
            socket.on(SocketEvent.PLAY, () => { this.playerevent.playVideo(socket); });
            socket.on(SocketEvent.STOP, () => { this.playerevent.stopVideo(socket); });
            socket.on(SocketEvent.PAUSE, () => { this.playerevent.pauseVideo(socket); });
            socket.on(SocketEvent.NEXT, () => { this.playerevent.playFromQueue(socket,0); });
            socket.on(SocketEvent.FINISH, () => { this.playerevent.videoFinished(socket); });
            socket.on(SocketEvent.ADD_TO_QUEUE, (vidId: string) => { this.playerevent.addVideoToQueue(socket, vidId); });
            socket.on(SocketEvent.PLAY_QUEUE_ELEM, (queueElem: number) => { this.playerevent.playFromQueue(socket, queueElem); });
            socket.on(SocketEvent.RM_QUEUE_ELEM, (queueElem: number) => { this.playerevent.removeVideoFromQueue(socket, queueElem); });

            // Chat Events
            socket.on(SocketEvent.MSG, (msg: string) => {
                let user = this.users.getUserBySocketId(socket.id);
                if(user){
                    let room = this.rooms.getRoom(user.getCurrentRoom());
                    this.chat.OnUserMessage(socket, msg, user, room);
                }
            });

            // API Events
            socket.on(SocketEvent.searchYT, async (searchTerm: string) => { this.apisearchVideos(socket, searchTerm); });
            socket.on(SocketEvent.playlistVideos, async (playlistId: string) => { this.apiGetPlaylistVideos(socket, playlistId); });
            socket.on(SocketEvent.LOAD_RELATED,async () => { this.playerevent.getRelatedVideos(socket); });
        });
    }

    private async apisearchVideos(socket: SocketIO.Socket, searchterm: string) {
        if (this.ytApi.ready && this.userevent.userApiRequestAllowed(socket)) {
            socket.emit(SocketEvent.searchYT, true, await this.ytApi.searchKeyWord(searchterm));
        }
        else {
            socket.emit(SocketEvent.MSG, Errors.APIREQUESTDENIED, SystemUserName.SYSTEM);
        }
    };

    private async apiGetPlaylistVideos(socket: SocketIO.Socket, playlistId:string){
        if(this.ytApi.ready && this.userevent.userApiRequestAllowed(socket)){
            socket.emit(SocketEvent.playlistVideos, await this.ytApi.getPlaylistVideos(playlistId));
        }
        else{
            socket.emit(SocketEvent.MSG, Errors.APIREQUESTDENIED, SystemUserName.SYSTEM);
        }
    }

    public getApp(): express.Application {
        return this.app;
    }
}

let appserver = new AppServer();
