import { ChatEvents } from "./ChatEvents";
import { UserData } from "./UserData";
import { RoomData } from "./RoomData";
import { Errors, SystemUserName, SocketEvent } from "./Enums";
import { youtubeapi } from "./youtubeapi";
import { Video } from "./Video";

export class PlayerEvents {
    private io: SocketIO.Server;
    private chat: ChatEvents;
    private rooms: RoomData;
    private users: UserData;
    private ytApi: youtubeapi;
    constructor(io: SocketIO.Server, chat: ChatEvents, rooms: RoomData, users: UserData, ytApi: youtubeapi) {
        this.io = io;
        this.chat = chat;
        this.rooms = rooms;
        this.users = users;
        this.ytApi = ytApi;
    }

    playVideo(socket: SocketIO.Socket) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        this.io.to(user.getCurrentRoom()).emit(SocketEvent.PLAY);
        this.chat.roomMsg(user.getCurrentRoom(), user.getUserName() + ' hit play Video', null);
    }

    pauseVideo(socket: SocketIO.Socket) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            if (room.getIsEditable() || room.isOwner(user.getUserName())) {
                this.io.to(user.getCurrentRoom()).emit(SocketEvent.PAUSE);
                this.chat.roomMsg(user.getCurrentRoom(), user.getUserName() + ' hit pause Video', null);
            } else {
                this.chat.socketMsg(socket, Errors.ROOMNOTEDITABLE, SystemUserName.SYSTEM);
            }
        }
    }

    stopVideo(socket: SocketIO.Socket) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            if (room.getIsEditable() || room.isOwner(user.getUserName())) {
                this.io.to(user.getCurrentRoom()).emit(SocketEvent.STOP);
                this.chat.roomMsg(user.getCurrentRoom(), user.getUserName() + ' hit stop Video', null);
            } else {
                this.chat.socketMsg(socket, Errors.ROOMNOTEDITABLE, SystemUserName.SYSTEM);
            }
        }
    }

    async loadVideo(socket: SocketIO.Socket, vidId: string) {
        if (!vidId || vidId.length === 0 || vidId.charAt(0) === "") {
            this.chat.socketMsg(socket, Errors.IDEMPTY, SystemUserName.SYSTEM);
            return;
        }
        let user = this.users.getUserBySocketId(socket.id)
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            if (room.getIsEditable() || room.isOwner(user.getUserName())) {
                let video = await this.apiGetVidInfo(vidId)
                room.setCurrentVideo(video);
                this.io.to(user.getCurrentRoom()).emit(SocketEvent.LOAD_VID, vidId);
                this.io.to(user.getCurrentRoom()).emit(SocketEvent.LOAD_QUEUE, room.getQueue(), room.getQueueCount());
                let vidInfo:Array<string> = new Array<string>(video.getName(),video.getChannel(),video.getViews(),video.getLikes(),video.getDislikes(),video.getPostedTime());
                this.io.to(user.getCurrentRoom()).emit(SocketEvent.VIDSTATS, vidInfo);
                this.chat.roomMsg(user.getCurrentRoom(), user.getUserName() + ' set new Video', null);
            } else {
                this.chat.socketMsg(socket, Errors.ROOMNOTEDITABLE, SystemUserName.SYSTEM);
            }
        }
    }

    videoFinished(socket: SocketIO.Socket) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room && room.vidFinish()) {
            this.playFromQueue(socket,0);
        }
    }

    addVideoToQueue(socket: SocketIO.Socket, vidId: string) {
        if (!vidId || vidId.length === 0 || vidId.charAt(0) === "") {
            this.chat.socketMsg(socket, Errors.IDEMPTY, SystemUserName.SYSTEM);
            return;
        }
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            if (room.getCurrentUrl() == null) {
                this.loadVideo(socket, vidId);
            }
            else if (room.getIsEditable() || room.isOwner(user.getUserName()) || room.getQueue.length >= this.rooms.queueLimit) {
                room.addToQueue(vidId);
                this.io.to(user.getCurrentRoom()).emit(SocketEvent.LOAD_QUEUE, room.getQueue(), room.getQueueCount());
                this.chat.roomMsg(user.getCurrentRoom(), user.getUserName() + ' added ' + vidId + ' to Videoqueue', null);
            } else {
                this.chat.socketMsg(socket, Errors.QUEUELIMIT, SystemUserName.SYSTEM);
            }
        }
    }

    removeVideoFromQueue(socket: SocketIO.Socket, qElem: number) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            room.removeFromQueue(qElem);
            this.io.to(user.getCurrentRoom()).emit(SocketEvent.LOAD_QUEUE, room.getQueue(), room.getQueueCount());
            this.chat.roomMsg(user.getCurrentRoom(), user.getUserName() + ' hit removed Video from Queue', null);
        }
    }

    playFromQueue(socket: SocketIO.Socket, qElem: number) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            if (room.getIsEditable() || room.isOwner(user.getUserName())) {
                let vidID = room.playFromQueue(qElem);
                this.loadVideo(socket,vidID);
                this.chat.roomMsg(user.getCurrentRoom(), user.getUserName() + ' hit set Video from Queue', null);
            } else {
                this.chat.socketMsg(socket, Errors.ROOMNOTEDITABLE, SystemUserName.SYSTEM);
            }
        }
    }

    loadQueue(socket: SocketIO.Socket) {
        let user = this.users.getUserBySocketId(socket.id);
        if (!user) {
            return
        }
        let room = this.rooms.getRoom(user.getCurrentRoom());
        if (room) {
            socket.emit(SocketEvent.LOAD_QUEUE, room.getQueue(), room.getQueueCount());
        }
    }

    async getRelatedVideos(socket: SocketIO.Socket) {
        let user = this.users.getUserBySocketId(socket.id);
        if (user && this.ytApi.ready) {
            let room = this.rooms.getRoom(user.getCurrentRoom());
            if (room) {
                room.setRelatedVids(await this.ytApi.getRelatedVids(room.getCurrentUrl()));
                this.io.to(user.getCurrentRoom()).emit(SocketEvent.RELATEDVIDS, room.getRelatedVids());
            }
            else {
                socket.emit(SocketEvent.MSG, "Something went wrong loading related Videos!", SystemUserName.SYSTEM)
            }
        }
        else {
            socket.emit(SocketEvent.MSG, "Something went wrong loading related Videos!", SystemUserName.SYSTEM)
        }
    };

    async apiGetVidInfo(vidId:string) {
        if (this.ytApi.ready) {
            return await this.ytApi.getVidInfo(vidId);
        }
        return new Video(vidId,null,null,null,null,null,null);
    };
}