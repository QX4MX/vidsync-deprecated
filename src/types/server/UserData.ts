import { User } from "./User";

export class UserData {
    private userMap: Map<string, User> = new Map();

    constructor() { }

    newUser(socketid: string, name: string): User {
        let user: User = new User(socketid, name);
        this.userMap.set(user.getUserId(), user);
        return user;
    }

    deleteUser(userid: string) {
        this.userMap.delete(userid);
    }

    getAll() {
        let userArr: Array<Array<string>> = new Array<Array<string>>();
        for (let [userid, user] of this.userMap) {
            let userStr: Array<string> = new Array<string>();
            userStr.push(user.getUserName());
            userStr.push(user.getCurrentSocketId());
            userStr.push(user.getCurrentRoom());
            userStr.push(String(user.getStatus()));
            userStr.push(user.getLastLogin().toLocaleString())
            userArr.push(userStr);
        }
        return userArr;
    }


    getUserBySocketId(socketId: string): User {
        for (let [userid, user] of this.userMap) {
            if (user.getCurrentSocketId() == socketId) {
                return user;
            }
        }
        return null;
    }

    getUserByUserName(username: string) {
        for (let [userid, user] of this.userMap) {
            if (user.getUserName() == username) {
                return user;
            }
        }
        return null;
    }

    usernameUsed(userid: string, username: string) {

        for (let [uid, user] of this.userMap) {
            if (user.getUserName().toUpperCase() == username.toUpperCase() && uid != userid) {
                return true;
            }
        }
        return false;
    }

    connect(userid: string, newSocketId: string, username: string) {
        let user = this.userMap.get(userid);
        if (user == null) {
            this.newUser(newSocketId, username);
            console.log('New Client connected : ' + username);
            user = this.userMap.get(userid);
            return;
        }
        if (username != user.getUserName()) {
            user.setUserName(username);
        }
        user.setCurrentSocketId(newSocketId);
        user.setLastLogin(new Date());
        user.setStatus(true);
    }

    disconnect(user: User) {
        user.setStatus(false);
        user.setCurrentRoom("");
    }
}
