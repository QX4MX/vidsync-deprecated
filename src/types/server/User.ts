export class User {
    private userid: string;
    private currentSocketId: string;
    private userName: string;
    private currentRoom: string = "";
    private online: boolean;
    private lastlogin: Date;
    private lastApiRequest: Date;
    private apiRequestCoolDown: number = 5000;
    constructor(currentSocketId: string, userName: string) {
        this.userid = '_' + Math.random().toString(36).substr(2, 9);
        this.setCurrentSocketId(currentSocketId);
        this.userName = userName;
        this.lastlogin = new Date();
    }

    public getUserId(): string { return this.userid; }

    public getCurrentSocketId(): string { return this.currentSocketId; }

    public getUserName(): string { return this.userName; }

    public getCurrentRoom(): string { return this.currentRoom; }

    public getStatus(): boolean { return this.online; }

    public getLastLogin(): Date { return this.lastlogin; }

    public setCurrentSocketId(socketId: string): void {
        this.currentSocketId = socketId;
    }

    public getAllowApiRequest() {
        if (this.lastApiRequest == null || this.lastApiRequest.getTime() <= new Date().getTime() - this.apiRequestCoolDown) {
            return true;
        }
        return false;
    }

    public setLastApiRequest() {
        this.lastApiRequest = new Date();
    }
    public setUserName(userName: string): void {
        console.log(this.userName, ' changed Name to: ' + userName);
        this.userName = userName;
    }

    public setCurrentRoom(currentRoom: string): void {
        this.currentRoom = currentRoom;
    }

    public setLastLogin(date: Date) {
        this.lastlogin = date;
    }

    public setStatus(bool: boolean) {
        this.online = bool;
    }
}