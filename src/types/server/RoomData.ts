import { Room } from "./Room";
import { Video } from "./Video";

export class RoomData {
    private roomMap: Map<string, Room> = new Map();
    private roomLimit: number = 500;
    public queueLimit: number = 20;

    constructor() {
    }

    newRoom(name: string, creator: string, isPerm: boolean, isPublic: boolean, isEditable: boolean, startvid: Video): Room {
        let room: Room = new Room(name, creator, isPerm, isPublic, isEditable, startvid);
        this.roomMap.set(name, room);
        return room;
    }

    createRoomIfNotExisting(roomID: string, creator: string, isPerm: boolean, isPublic: boolean, isEditable: boolean, startVidId: Video): Room {
        let room = this.getRoom(roomID);
        if (!room && this.getRoomCount() <= this.roomLimit) {
            this.newRoom(roomID, creator, isPerm, isPublic, isEditable, startVidId);
            room = this.getRoom(roomID);
        }
        return room;
    }

    getRoom(roomId: string): Room {
        let room = this.roomMap.get(roomId);
        return room;
    }

    getAll() {
        let roomIds: Array<Array<string>> = new Array<Array<string>>();
        for (let [roomId, room] of this.roomMap) {
            let roomStr: Array<string> = new Array<string>();
            roomStr.push(room.getName());
            roomStr.push(room.getCreatedBy());
            roomStr.push(room.getUserCount().toString());
            roomStr.push(room.getCurrentUrl());
            roomStr.push(room.getQueueCount().toString());
            roomStr.push(room.getIsPublic().toString());
            roomStr.push(room.getIsPerm().toString());
            roomStr.push(room.getIsEditable().toString());
            roomStr.push(room.getLastUsed().toLocaleString());
            roomIds.push(roomStr);
        }
        return roomIds;
    }

    getRoomCount() {
        return this.roomMap.size;
    }

    deleteRoom(roomId: string): void {
        this.roomMap.delete(roomId);
    }

    getPublicRooms() {
        let roomIds: Array<Array<string>> = new Array<Array<string>>();
        for (let [roomId, room] of this.roomMap) {
            if (room.getIsPublic() == true) {
                let roomStr: Array<string> = new Array<string>();
                roomStr.push(room.getName());
                roomStr.push(room.getCreatedBy());
                roomStr.push(room.getCurrentUrl());
                roomStr.push(room.getUserCount().toString());
                roomStr.push(room.getIsPerm().toString());
                roomStr.push(room.getIsEditable().toString());
                roomIds.push(roomStr);
            }
        }
        return roomIds;
    }

    deleteInactiveRooms(): void {
        let currentdate = new Date();
        for (let [roomId, room] of this.roomMap) {
            if (room.checkIfActive() == false) {
                this.deleteRoom(roomId);
                console.log('Deleted Room: ', roomId)
            }
        }
    }

    updateRoomOwnerUserName(newUserName: string, oldUserName: string) {
        for (let [roomId, room] of this.roomMap) {
            if (room.getCreatedBy() == oldUserName) {
                room.setCreatedBy(newUserName);
            }
        }
    }

}
