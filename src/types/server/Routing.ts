import * as path from "path";
import { Admin } from "./Admin";
import { youtubeapi } from "./youtubeapi";
export class Routing {
    private admin: Admin
    private ytApi: youtubeapi;

    constructor(admin: Admin, api: youtubeapi) {
        this.admin = admin;
        this.ytApi = api;
    }

    get(req: any, res: any) {
        var route = req.path;
        if (route == '/') {
            res.sendFile(path.resolve('./dist/public/resources/html/index.html'));
        }
        else if (route == '/privacy' || route == '/room/') {
            res.sendFile(path.resolve('./dist/public/resources/html/privacy.html'));
        }
        else if (route == '/room' || route == '/room/') {
            res.sendFile(path.resolve('./dist/public/resources/html/room.html'));
        }
        else if (route == '/admin') {
            if (!this.admin.checkPassword(req.query.psw)) {
                res.sendFile(path.resolve('./dist/public/resources/html/login.html'));
            }
            else {
                res.sendFile(path.resolve('./dist/public/resources/html/admin.html'));
            }
        }
        else if (route == '/admin/setpw') {
            if (this.admin.checkPassword(req.query.opsw) && req.query.npsw1 == req.query.npsw2 && req.query.npsw1 != null && req.query.npsw2 != null) {
                if (this.admin.setPassword(req.query.opsw, req.query.npsw1)) {
                    res.redirect("/admin?psw=" + req.query.pw + "");
                }
                else {
                    res.send('fail');
                }
            }
        }
        else if (route == '/admin/setApiKey') {
            if (this.admin.checkPassword(req.query.pw)) {
                if (this.ytApi.setApiKey(req.query.apikey)) {
                    console.log("Api Key has been set!");
                    res.redirect("/admin?psw=" + req.query.pw + "");
                }
                else {
                    res.send('fail');
                }
            }
        }
        else {
            res.send("404");
        }
    }
    post(req: any, res: any) {
        res.send("404 / 405")
    }
}
